<?php
require 'database.php';

// Get the posted data.
$postdata = file_get_contents("php://input");

if(isset($postdata) && !empty($postdata))
{
  // Extract the data.
  $request = json_decode($postdata);


  // Validate.
  if(!$request->receipt_id || !$request->item_id)
  {
    return http_response_code(400);
  }
  
  $policy = R::dispense('receipt_items');
  $policy->receipt_id = trim($request->receipt_id);
  $policy->item_id = trim($request->item_id);
  R::store($policy);
}
?>