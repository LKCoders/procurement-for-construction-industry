<?php
require 'database.php';

// Get the posted data.
$postdata = file_get_contents("php://input");

if(isset($postdata) && !empty($postdata))
{
  // Extract the data.
  $request = json_decode($postdata);

  // Validate.
  if (!$request->id || !$request->order_id || !$request->note) {
    return http_response_code(400);
  }
    
  $order_receipt = R::load('order_receipts', (int)$request->id);
  $order_receipt->note = trim($request->note);
  R::store($order_receipt);
}
?>