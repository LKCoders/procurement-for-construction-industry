<?php
/**
 * Returns the list of items.
 */
require 'database.php';

$receipt_id = (int)$_GET['rid'];
$items = [];
$result = R::getAll("SELECT RIT.*, ITM.name as item_name, ORD.purchase_number as purchase_number FROM receipt_items RIT, items ITM, orders ORD
    WHERE 
        ORD.id = (select order_receipts.order_id  from order_receipts where order_receipts.id = $receipt_id) AND
		RIT.receipt_id = $receipt_id AND
		RIT.item_id = ITM.id");

$bItems = R::convertToBeans('bItems', $result); 

if ($bItems)
{
	$i = 0;
	foreach ($bItems as $item) {
        $items[$i]['id']    = $item->id;
		$items[$i]['receipt_id'] = $item->id;
		$items[$i]['item_id'] = $item->item_id;
		$items[$i]['inspection_result_id']    = $item->inspection_result_id;
		$items[$i]['inspection_note'] = $item->inspection_note;
		$items[$i]['item_name'] = $item->item_name;
		$items[$i]['purchase_number'] = $item->purchase_number;
		$i++;
    }
	echo json_encode($items);
}
else
{
	http_response_code(404);
}

?>