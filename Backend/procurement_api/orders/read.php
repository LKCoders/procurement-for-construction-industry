<?php
/**
 * Returns the list of orders.
 */
require 'database.php';

$orders = [];
$result = R::getAll("SELECT ORD.*, SI.name as site_name, SU.name as supplier_name FROM orders ORD, sites SI , suppliers SU
    WHERE 
        ORD.site_id = SI.id AND
        ORD.supplier_id = SU.id");

$bOrders = R::convertToBeans('bOrders', $result); 

if ($bOrders)
{
	$i = 0;
	foreach ($bOrders as $order) {
        $orders[$i]['id']    = $order->id;
		$orders[$i]['site_id '] = $order->site_id ;
		$orders[$i]['purchase_number'] = $order->purchase_number;
		$orders[$i]['supplier_id ']    = $order->supplier_id ;
		$orders[$i]['name'] = $order->name;
		$orders[$i]['description'] = $order->description;
		$orders[$i]['order_date']    = $order->order_date;
		$orders[$i]['received_date'] = $order->received_date;
		$orders[$i]['delivery_address'] = $order->delivery_address;
		$orders[$i]['is_deleted']    = $order->is_deleted;
		$orders[$i]['is_deleted']    = $order->is_deleted;
		$orders[$i]['site_name']    = $order->site_name;
		$orders[$i]['supplier_name']    = $order->supplier_name;
		$i++;
    }
	echo json_encode($orders);
}
else
{
	http_response_code(404);
}

?>