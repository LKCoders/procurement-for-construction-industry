<?php
/**
 * Returns the list of receipts.
 */
require 'database.php';

$order_id = (int)$_GET['oid'];
$order_receipts = [];

$result = R::getAll("SELECT ODR.*, O.purchase_number as purchase_number FROM order_receipts ODR, orders O
    WHERE 
        O.id = $order_id AND
		O.id = ODR.order_id");
	
$bOrder_Receipts = R::convertToBeans('bOrder_Receipts', $result);

if($bOrder_Receipts){
	
	$i = 0;
	
	foreach ($bOrder_Receipts as $order_receipt) {
		$order_receipts[$i]['id'] = $order_receipt->id;
		$order_receipts[$i]['order_id'] = $order_receipt->order_id;
		$order_receipts[$i]['receipt_date'] = $order_receipt->receipt_date;
		$order_receipts[$i]['note'] = $order_receipt->note;
		$order_receipts[$i]['purchase_number'] = $order_receipt->purchase_number;
		$i++;
	}
	echo json_encode($order_receipts);
}
else{
	http_response_code(404);
}

?>