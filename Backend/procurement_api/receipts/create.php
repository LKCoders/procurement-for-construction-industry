<?php
require 'database.php';

// Get the posted data.
$postdata = file_get_contents("php://input");

if(isset($postdata) && !empty($postdata))
{
  // Extract the data.
  $request = json_decode($postdata);


  // Validate.
  if(!$request->order_id)
  {
    return http_response_code(400);
  }
  
  $policy = R::dispense('order_receipts');
  $policy->order_id = trim($request->order_id);
  R::store($policy);
}
?>