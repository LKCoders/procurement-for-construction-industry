import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Item } from '../item';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  items:  Item[];

  selectedItem:  Item  = { id :  null , receipt_id: null, item_id: null, inspection_result_id: null, inspection_note: null,
    item_name: null, purchase_number: null};

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.readItems(1).subscribe((items: Item[])=>{
      this.items = items;
      console.log(this.items);
    })
  }

  selectOrder(item: Item){
    this.selectedItem = item;
  }


}
