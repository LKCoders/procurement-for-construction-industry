export  class  Receipt {
    id: number;
    order_id: number;
    receipt_date: Date;
    note: string;
    purchase_number: string;
}