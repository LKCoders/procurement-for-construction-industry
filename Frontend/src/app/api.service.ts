import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Policy } from  './policy';
import { Order } from  './order';
import { Item } from  './item';
import { Receipt } from  './receipt';
import { Observable } from  'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  PHP_API_SERVER = "http://localhost";
  constructor(private httpClient: HttpClient) {}

  readPolicies(): Observable<Policy[]>{
    return this.httpClient.get<Policy[]>(`${this.PHP_API_SERVER}/api/read.php`);
  }

  createPolicy(policy: Policy): Observable<Policy>{
    return this.httpClient.post<Policy>(`${this.PHP_API_SERVER}/api/create.php`, JSON.stringify(policy));
  }

  updatePolicy(policy: Policy){   
    return this.httpClient.put<Policy>(`${this.PHP_API_SERVER}/api/update.php`, JSON.stringify(policy));   
  }

  deletePolicy(id: number){
    return this.httpClient.delete<Policy>(`${this.PHP_API_SERVER}/api/delete.php/?id=${id}`);
  }

  //Orders
  readOrders(): Observable<Order[]>{
    return this.httpClient.get<Order[]>(`${this.PHP_API_SERVER}/procurement_api/orders/read.php`);
  }

  //Items
  readItems(receiptID: number): Observable<Item[]>{
    return this.httpClient.get<Item[]>(`${this.PHP_API_SERVER}/procurement_api/items/read.php/?rid=${receiptID}`);
  }

  createItem(item: Item): Observable<Receipt>{
    console.log("createItem", JSON.stringify(item));
    return this.httpClient.post<Receipt>(`${this.PHP_API_SERVER}/procurement_api/items/create.php`, JSON.stringify(item));
  }

  //Receipts
  readReceipts(orderID: number): Observable<Receipt[]>{
    return this.httpClient.get<Receipt[]>(`${this.PHP_API_SERVER}/procurement_api/receipts/read.php?oid=${orderID}`);
  }

  createReceipt(receipt: Receipt): Observable<Receipt>{
    console.log("createReceipt", JSON.stringify(receipt));
    return this.httpClient.post<Receipt>(`${this.PHP_API_SERVER}/procurement_api/receipts/create.php`, JSON.stringify(receipt));
  }

  updateReceipt(receipt: Receipt){   
    return this.httpClient.put<Receipt>(`${this.PHP_API_SERVER}/procurement_api/receipts/update.php`, JSON.stringify(receipt));   
  }
}