import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Order } from '../order';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  orders:  Order[];
  selectedOrder:  Order  = { id :  null , site_id: null, purchase_number: null, supplier_id: null, name: null, description: null, order_date: null, 
    received_date: null, delivery_address: null, is_deleted: null, site_name: null, supplier_name: null};

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.readOrders().subscribe((orders: Order[])=>{
      this.orders = orders;
      console.log(this.orders);
    })
  }

  selectOrder(order: Order){
    this.selectedOrder = order;
  }

}
