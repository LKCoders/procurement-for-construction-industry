import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Receipt } from '../receipt';
import { NgForm }   from '@angular/forms';

@Component({
  selector: 'app-receipts',
  templateUrl: './receipts.component.html',
  styleUrls: ['./receipts.component.css']
})
export class ReceiptsComponent implements OnInit {

  receipts:  Receipt[];
  
  selectedReceipt:  Receipt  = { id :  null , order_id: null, receipt_date: null, note: null, purchase_number: null};

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.readReceipts(1).subscribe((receipts: Receipt[])=>{
      this.receipts = receipts;
      console.log(this.receipts);
    })
  }

  createOrUpdateReceipt(form: NgForm){
    
    if(this.selectedReceipt && this.selectedReceipt.id){
      
      form.value.id = this.selectedReceipt.id;      
      this.apiService.updateReceipt(form.value).subscribe((receipt: Receipt)=>{
        console.log("Policy updated" , receipt);
      });
    }
    else{      
      this.apiService.createReceipt(form.value).subscribe((receipt: Receipt)=>{
        console.log("Policy created, ", receipt);
      });
    }
    location.reload();

  }

  selectOrder(receipt: Receipt){
    this.selectedReceipt = receipt;
  }

}
