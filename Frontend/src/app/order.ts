export  class  Order {
    id: number;
    site_id: number;
    purchase_number: string;
    supplier_id: number;
    name: string;
    description: string;
    order_date: Date;
    received_date: Date;
    delivery_address: string;
    is_deleted: boolean;
    site_name: string;
    supplier_name: string;
}