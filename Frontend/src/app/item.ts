export  class  Item {
    id: number;
    receipt_id: number;
    item_id: number;
    inspection_result_id: number;
    inspection_note: string;
    item_name: string;
    purchase_number: string;
}